package android.com.saccoapp.services;

import android.com.saccoapp.model.Constants;
import android.com.saccoapp.model.Contribution;
import android.com.saccoapp.model.Loan;
import android.com.saccoapp.model.Member;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class GetData {

    private byte[] getBytes(String _url) throws IOException {
        String url = Constants.getBaseUrl() + _url;
        HttpURLConnection connection = HttpConnection.getUrl(url);

        try {

            ByteArrayOutputStream out = new ByteArrayOutputStream();
            InputStream in = connection.getInputStream();

            if (connection.getResponseCode() != HttpURLConnection.HTTP_OK) {
                throw new IOException(connection.getResponseMessage() + ":with " + url);
            }
            int bytesRead = 0;
            byte[] buffer = new byte[1024];
            while ((bytesRead = in.read(buffer)) > 0) {
                out.write(buffer, 0, bytesRead);
            }
            out.close();
            return out.toByteArray();
        } finally {
            connection.disconnect();
        }
    }

    public String getUrlString(String urlSpec) throws IOException {
        return new String(getBytes(urlSpec));
    }

    public String loginUser(String email, String password) {
        try {
            String data = "?login=saccoApp&email=" + URLEncoder.encode(email, "UTF-8");
            data += "&pass=" + URLEncoder.encode(password, "UTF-8");

            System.out.println("mutuma " + data);
            return getUrlString(data);
        } catch (UnsupportedEncodingException e) {
            return "error 1" + e.getMessage();
        } catch (IOException e) {
            return "error 2" + e.getMessage();
        }
    }

    public List<Member> getMembers() {
        List<Member> members = new ArrayList<>();
        try {
            String data = "?get_members=" + URLEncoder.encode("GET_MEMBERS", "UTF-8");

            String jsonData = getUrlString(data);

            JSONObject jsonObject = new JSONObject(jsonData);

            JSONArray array = jsonObject.getJSONArray("members");
            for (int i = 0; i < array.length(); i++) {
                JSONObject membersObject = array.getJSONObject(i);
                Member member = new Member();
                member.setMemberName(membersObject.getString("name"));
                member.setStatus(membersObject.getString("status"));
                member.setMemberReg(membersObject.getString("member_reg"));

                members.add(member);
            }
        } catch (UnsupportedEncodingException e) {
            return members;
        } catch (IOException e) {
            return members;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return members;
    }

    public Map<String, List<Object>> getLoans(String memberReg) {
        Map<String, List<Object>> objects = new HashMap<>();
        List<Object> loanList = new ArrayList<>();
        List<Object> contributionList = new ArrayList<>();
        int amount = 0;
        try {
            String data = "?loans=" + URLEncoder.encode("GET_LOANS", "UTF-8");
            data += "&member_reg=" + URLEncoder.encode(memberReg, "UTF-8");

            String jsonData = getUrlString(data);

            JSONObject jsonObject = new JSONObject(jsonData);
            if(jsonObject.length()>0) {
                JSONArray array = jsonObject.getJSONArray("loans");
                for (int i = 0; i < array.length(); i++) {
                    JSONObject loansObject = array.getJSONObject(i);
                    Loan loan = new Loan();
                    loan.setAmount(loansObject.getString("amount"));
                    loan.setLoanDate(loansObject.getString("due_date"));
                    amount = amount + Integer.parseInt(loansObject.getString("balance"));
                    loan.setBalance(String.valueOf(amount));

                    loanList.add(loan);
                }
            }
            String Cdata = "?contributions=" + URLEncoder.encode("GET_LOANS", "UTF-8");
            Cdata += "&member_reg=" + URLEncoder.encode(memberReg, "UTF-8");

            String DjsonData = getUrlString(Cdata);

            JSONObject CjsonObject = new JSONObject(DjsonData);

            int contributionAmount = 0;
            if(jsonObject.length()>0) {
                JSONArray Carray = CjsonObject.getJSONArray("contributions");
                for (int i = 0; i < Carray.length(); i++) {
                    JSONObject contributionsObject = Carray.getJSONObject(i);
                    Contribution contribution = new Contribution();
                    contributionAmount = contributionAmount + Integer.parseInt(contributionsObject.getString("amount"));
                    contribution.setAmount(String.valueOf(contributionAmount));
                    contribution.setDate(contributionsObject.getString("contribution_date"));

                    contributionList.add(contribution);
                }
            }
        } catch (UnsupportedEncodingException e) {
            return null;
        } catch (IOException e) {
            return null;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        objects.put("loans", loanList);
        objects.put("contributions",contributionList);

        return objects;
    }
}
