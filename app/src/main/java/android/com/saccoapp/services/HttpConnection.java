package android.com.saccoapp.services;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by bryan on 1/19/2017.
 */

public class HttpConnection
{
    public static HttpURLConnection getUrl(String urlSpec){
        try {
            URL url=new URL(urlSpec);
            HttpURLConnection urlConnection= (HttpURLConnection) url.openConnection();

            return urlConnection;
        } catch (MalformedURLException e) {
            e.printStackTrace();
            return null;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
}
