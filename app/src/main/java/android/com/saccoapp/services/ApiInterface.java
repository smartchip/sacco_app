package android.com.saccoapp.services;

import android.support.annotation.NonNull;

import retrofit2.Call;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ApiInterface {
    @NonNull
    @FormUrlEncoded
    @GET("login")
    Call<String> loginUser(@Query("user_email") String userEmail,@Query("user_pass") String password);

   @GET("get_members")
    Call<String> getMembers(@Query("key") String userKey);

}
