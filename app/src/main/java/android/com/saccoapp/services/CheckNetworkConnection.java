package android.com.saccoapp.services;

import android.com.saccoapp.model.Constants;
import android.content.Context;
import android.net.ConnectivityManager;
import android.util.Log;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;


public class CheckNetworkConnection
{
    private static boolean isNetworkAvailableAndConnected(Context context){
        ConnectivityManager connectivityManager=(ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);

        boolean isNetworkAvailable=connectivityManager.getActiveNetworkInfo()!=null;
        boolean isNetworkConnected=isNetworkAvailable&&connectivityManager.getActiveNetworkInfo()!=null;

        return isNetworkConnected;

    }
    public static boolean isURLReachable(Context context){
        if(isNetworkAvailableAndConnected(context)){
            try {
                String url= Constants.getBaseUrl();
                HttpURLConnection connection=HttpConnection.getUrl(url);
                connection.setConnectTimeout(4 *1000);
                Log.i("Connection",String.format("%s",connection.getResponseCode()));

                if(connection.getResponseCode()==HttpURLConnection.HTTP_OK){
                    Log.i("Connection","success");
                    return true;
                }else{
                    Log.i("Connection","connection not successfull");
                    return false;
                }
            } catch (MalformedURLException e) {
                Log.e("Connection","connection not successfull"+e);
                return false;
            } catch (IOException e) {
                Log.e("Connection","connection not successfull"+e);
                return false;
            }
        }
        Log.e("Connection","network not available");
        return false;
    }
}
