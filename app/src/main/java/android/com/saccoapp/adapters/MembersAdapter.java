package android.com.saccoapp.adapters;

import android.com.saccoapp.R;
import android.com.saccoapp.model.Member;
import android.com.saccoapp.utilities.ColorGenerator;
import android.com.saccoapp.utilities.TextDrawable;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class MembersAdapter extends RecyclerView.Adapter<MembersAdapter.MembersViewHolder>{
    private List<Member> membersList;
    private Context mContext;

    public MembersAdapter(List<Member> membersList, Context context) {
        this.membersList = membersList;
        mContext = context;
    }

    @Override
    public MembersViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.member_layout,null);
        return new MembersViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MembersViewHolder holder, int position) {
        Member member=membersList.get(position);
        holder.bindUser(member);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }

    @Override
    public int getItemCount() {
        return membersList.size();
    }
    public void searchMember(String criteria){
        List<Member> members=new ArrayList<>();
        for (int i = 0; i < membersList.size(); i++) {
            Member member=membersList.get(i);
            if(member.getMemberName().toLowerCase().contains(criteria.toLowerCase())||
                member.getStatus().contains(criteria.toLowerCase())){
                members.add(member);

            }
        }
        if(members.size()>0){
            membersList.clear();
            membersList=members;
            notifyDataSetChanged();
        }
    }

    public class MembersViewHolder extends RecyclerView.ViewHolder{
        private ImageView userInitials;
        private TextView userName,status;
        public MembersViewHolder(View itemView) {
            super(itemView);
            userInitials=itemView.findViewById(R.id.user_initials);
            userName=itemView.findViewById(R.id.user_name);
            status=itemView.findViewById(R.id.member_status);
        }
        void bindUser(Member member){
            String letter;
            String memberName=member.getMemberName().toUpperCase();
            if(memberName.contains(" "))
                letter=String.valueOf(memberName.split(" ")[0].charAt(0))+String.valueOf(memberName.split(" ")[1].charAt(0));
            else
                letter=String.valueOf(memberName.charAt(0))+String.valueOf(memberName.charAt(1));
            TextDrawable drawable= TextDrawable.builder().buildRound(letter, ColorGenerator.MATERIAL.getRandomColor());
            userInitials.setImageDrawable(drawable);
            userName.setText(member.getMemberName());
            status.setText(member.getStatus());
        }
    }
}
