package android.com.saccoapp.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Member implements Serializable{
    @SerializedName("name")
    private String memberName;
    @SerializedName("member_reg")
    private String memberReg;
    @SerializedName("account_no")
    private String accountNo;
    @SerializedName("status")
    private String status;
    @SerializedName("dob")
    private String dob;
    @SerializedName("phone_num")
    private String phoneNum;


    public String getMemberName() {
        return memberName;
    }

    public void setMemberName(String memberName) {
        this.memberName = memberName;
    }

    public String getMemberReg() {
        return memberReg;
    }

    public void setMemberReg(String memberReg) {
        this.memberReg = memberReg;
    }

    public String getAccountNo() {
        return accountNo;
    }

    public void setAccountNo(String accountNo) {
        this.accountNo = accountNo;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getPhoneNum() {
        return phoneNum;
    }

    public void setPhoneNum(String phoneNum) {
        this.phoneNum = phoneNum;
    }
}