package android.com.saccoapp;

import android.app.ProgressDialog;
import android.com.saccoapp.model.Member;
import android.com.saccoapp.services.CheckNetworkConnection;
import android.com.saccoapp.services.GetData;
import android.com.saccoapp.utilities.ColorGenerator;
import android.com.saccoapp.utilities.DividerDecoration;
import android.com.saccoapp.utilities.TextDrawable;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MembersListActivity extends AppCompatActivity {
    private MembersAdapter mMembersAdapter;
    private List<Member> mMemberList;
    private RecyclerView membersRecylerView;


    public static Intent newInstance(Context context){
        Intent intent=new Intent(context,MembersListActivity.class);
        return intent;
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_members_list);
        Toolbar toolbar=findViewById(R.id.members_tool_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        getSupportActionBar().setTitle("Members");
        membersRecylerView=findViewById(R.id.members_list);
        membersRecylerView.setLayoutManager(new LinearLayoutManager(this));
        membersRecylerView.addItemDecoration(new DividerDecoration(this, LinearLayoutManager.VERTICAL));

        mMemberList=new ArrayList<>();


    }

    @Override
    protected void onStart() {
        super.onStart();
        new MembersAsyncTask().execute();
    }

    //load data
    private void loadData(){
        for (int i = 0; i < 10; i++) {
            Member member=new Member();
            member.setMemberName("Member "+(i+1));
            member.setStatus(i%2==0?"Active":"Dormant");
            mMemberList.add(member);
        }
        mMembersAdapter.notifyDataSetChanged();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.search_members,menu);

        MenuItem searchItem = menu.findItem(R.id.member_search);
        searchItem.setTitle("Search Members");

        SearchView searchView= (SearchView) searchItem.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                mMembersAdapter.searchMember(newText);
                return true;
            }
        });
        return super.onCreateOptionsMenu(menu);
    }
     class MembersAdapter extends RecyclerView.Adapter<MembersAdapter.MembersViewHolder>{
        private List<Member> membersList;

         MembersAdapter(List<Member> membersList) {
            this.membersList = membersList;
        }

        @Override
        public MembersViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.member_layout,null);
            return new MembersViewHolder(view);
        }

        @Override
        public void onBindViewHolder(MembersAdapter.MembersViewHolder holder, int position) {
            final Member member=membersList.get(position);
            holder.bindUser(member);
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    new GetLoansAsyncTask(member           ).execute();
                }
            });
        }

        @Override
        public int getItemCount() {
            return membersList.size();
        }
        public void searchMember(String criteria){
            List<Member> members=new ArrayList<>();
            for (int i = 0; i < membersList.size(); i++) {
                Member member=membersList.get(i);
                if(member.getMemberName().toLowerCase().contains(criteria.toLowerCase())||
                        member.getStatus().contains(criteria.toLowerCase())){
                    members.add(member);

                }
            }
            if(members.size()>0){
                membersList.clear();
                membersList=members;
                notifyDataSetChanged();
            }
        }

         class MembersViewHolder extends RecyclerView.ViewHolder{
            private ImageView userInitials;
            private TextView userName,status;
            public MembersViewHolder(View itemView) {
                super(itemView);
                userInitials=itemView.findViewById(R.id.user_initials);
                userName=itemView.findViewById(R.id.member_name);
                status=itemView.findViewById(R.id.member_status);
            }
            void bindUser(Member member){
                String letter;
                String memberName=member.getMemberName().toUpperCase();
                if(memberName.contains(" "))
                    letter=String.valueOf(memberName.split(" ")[0].charAt(0))+String.valueOf(memberName.split(" ")[1].charAt(0));
                else
                    letter=String.valueOf(memberName.charAt(0))+String.valueOf(memberName.charAt(1));
                TextDrawable drawable= TextDrawable.builder().buildRound(letter, ColorGenerator.MATERIAL.getRandomColor());
                userInitials.setImageDrawable(drawable);
                userName.setText(member.getMemberName());
                status.setText(member.getStatus());

                if(member.getStatus().equals("Dormant")){
                    status.setTextColor(Color.RED);
                }else{
                    status.setTextColor(getResources().getColor(android.R.color.holo_green_dark));
                }
            }
        }
    }

    private class MembersAsyncTask extends AsyncTask<Void,Void,Object>{
        private ProgressDialog dialog;

        public MembersAsyncTask() {
            this.dialog = new ProgressDialog(MembersListActivity.this);
        }

        @Override
        protected void onPreExecute() {
            dialog.setMessage("Getting members");
            dialog.show();
        }

        @Override
        protected Object doInBackground(Void... voids) {
            if(CheckNetworkConnection.isURLReachable(MembersListActivity.this)){
                return new GetData().getMembers();
            }else{
                return false;
            }
        }

        @Override
        protected void onPostExecute(Object o) {
            dialog.dismiss();
            if(o instanceof Boolean){
                Toast.makeText(MembersListActivity.this, "No Internet. Please check your network settings", Toast.LENGTH_SHORT).show();
            }else{
                List<Member> members=(List<Member>)o;
                mMembersAdapter=new MembersAdapter(members);
                membersRecylerView.setAdapter(mMembersAdapter);

                System.out.println("mutuma "+members.size());
            }
        }
    }
    private class GetLoansAsyncTask extends AsyncTask<Void,Void,Object>{
        private Member member;
        private ProgressDialog dialog;

        public GetLoansAsyncTask(Member member) {
            this.member = member;
            this.dialog = new ProgressDialog(MembersListActivity.this);
        }

        @Override
        protected void onPreExecute() {
            dialog.setMessage("Getting members");
            dialog.show();
        }

        @Override
        protected Object doInBackground(Void... voids) {
            if(CheckNetworkConnection.isURLReachable(MembersListActivity.this)){
                return new GetData().getLoans(member.getMemberReg());
            }else{
                return false;
            }
        }

        @Override
        protected void onPostExecute(Object o) {
            dialog.dismiss();
            if(o instanceof Boolean){
                Toast.makeText(MembersListActivity.this, "No Internet. Please check your network settings", Toast.LENGTH_SHORT).show();
            }else{
                Map<String,List<Object>> listMap= (Map<String, List<Object>>) o;
                startActivity(UserDetailsActivity.newInstance(MembersListActivity.this,member,listMap));
            }
        }
    }
}
