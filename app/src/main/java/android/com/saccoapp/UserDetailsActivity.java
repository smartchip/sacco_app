package android.com.saccoapp;

import android.com.saccoapp.fragment.ContributionFragment;
import android.com.saccoapp.fragment.LoanFragment;
import android.com.saccoapp.model.Contribution;
import android.com.saccoapp.model.Loan;
import android.com.saccoapp.model.Member;
import android.com.saccoapp.utilities.ColorGenerator;
import android.com.saccoapp.utilities.TextDrawable;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class UserDetailsActivity extends AppCompatActivity {
    public static final String MEMBER="member";
    public static final String LOAN="loan";

    private Member member;
    private static Map<String,List<Object>> listMap;
    private Loan loan;
    private List<Object> loanList;
    private List<Object> contributions;
    private Contribution contribution;



    public static Intent newInstance(Context context, Member member, Map<String,List<Object>> items){
        Intent intent=new Intent(context,UserDetailsActivity.class);
        intent.putExtra(MEMBER,member);
        listMap=items;
        return intent;
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_details);

        Toolbar toolbar=findViewById(R.id.user_details_toolbar);
        setSupportActionBar(toolbar);
        member= (Member) getIntent().getSerializableExtra(MEMBER);
        loanList= listMap.get("loans");
        contributions=listMap.get("contributions");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(member.getMemberName());

        ImageView userInitials=findViewById(R.id.user_initials);
        Button loanBtn=findViewById(R.id.pay_loan_btn);
        Button contributionsBtn=findViewById(R.id.contribution_btn);
        TextView loanAmount=findViewById(R.id.loan_amount);
        TextView contributionAmount=findViewById(R.id.contribution_amount);
        String letter;

        loanBtn.setVisibility(View.INVISIBLE);

        loanAmount.setText("Kes: 0");
        contributionAmount.setText("Kes: 0");
        if(loanList.size()>0){
            Loan loan= (Loan) loanList.get(0);
            if(Integer.parseInt(loan.getBalance())>0){
                loanAmount.setText("Kes: "+loan.getBalance());
                loanBtn.setVisibility(View.VISIBLE);
            }
        }if(contributions.size()>0){
            Contribution contribution= (Contribution) contributions.get(0);
            if(Integer.parseInt(contribution.getAmount())>0){
                contributionAmount.setText("Kes: "+contribution.getAmount());
            }
        }
        String memberName=member.getMemberName().toUpperCase();
        if(memberName.contains(" "))
            letter=String.valueOf(memberName.split(" ")[0].charAt(0))+String.valueOf(memberName.split(" ")[1].charAt(0));
        else
            letter=String.valueOf(memberName.charAt(0))+String.valueOf(memberName.charAt(1));

        TextDrawable drawable= TextDrawable.builder().buildRound(letter, ColorGenerator.MATERIAL.getRandomColor());
        userInitials.setImageDrawable(drawable);

        ViewPager viewPager = findViewById(R.id.viewpager);
        setupViewPager(viewPager);

        TabLayout tabLayout=findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);

        FloatingActionButton button=findViewById(R.id.feedback);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showSendFeedback();
            }
        });

        TextView status=findViewById(R.id.user_status_text_view);
        status.setText(member.getStatus());
        if(member.getStatus().equals("Dormant")){
            status.setTextColor(Color.RED);
        }else{
            status.setTextColor(getResources().getColor(android.R.color.holo_green_dark));
        }
    }
    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFrag(new LoanFragment(), "Loans");
        adapter.addFrag(new ContributionFragment(), "CONTRIBUTIONS");

        viewPager.setAdapter(adapter);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        void addFrag(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    private void showSendFeedback(){
        View view= LayoutInflater.from(this).inflate(R.layout.comment_layout,null);
        final AlertDialog alertDialog = new AlertDialog.Builder(this)
                .setView(view)
                .setTitle("Feedback")
                .setPositiveButton("Send", null)
                .setNegativeButton("Cancel", null)
                .create();

        TextView feedbackTextView=view.findViewById(R.id.feedback_text_view);

        feedbackTextView.setText("Received feedback from "+member.getMemberName());
        alertDialog.show();
    }
}
