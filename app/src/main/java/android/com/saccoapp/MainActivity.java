package android.com.saccoapp;

import android.app.ProgressDialog;
import android.com.saccoapp.services.ApiClient;
import android.com.saccoapp.services.ApiInterface;
import android.com.saccoapp.services.CheckNetworkConnection;
import android.com.saccoapp.services.GetData;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


public class MainActivity extends AppCompatActivity {

    private ApiInterface apiInterface;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

       // apiInterface=ApiClient.getApiClient().create(ApiInterface.class);
        final EditText userName=findViewById(R.id.user_name);
        final EditText password=findViewById(R.id.password);


        Button loginBtn=findViewById(R.id.login_btn);

        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validateUser(userName,password);
            }
        });
    }

    private void validateUser(EditText userName, EditText password) {
        boolean hasError=false;
        if(userName.getText().toString().equals("")){
            userName.setError("email required");
            hasError=true;
        }
        if(password.getText().toString().equals("")){
            password.setError("password required");
            hasError=true;
        }

        if(!hasError){
            new LoginAsyncTask(userName.getText().toString(),password.getText().toString()).execute();

        }
    }
    private class LoginAsyncTask extends AsyncTask<Void,Void,Object>{
        private String email,password;
        private ProgressDialog progressDialog;

        public LoginAsyncTask(String email, String password) {
            this.email = email;
            this.password = password;
            progressDialog=new ProgressDialog(MainActivity.this);
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.setMessage("Logging you in..");
            progressDialog.show();
        }

        @Override
        protected Object doInBackground(Void... voids) {
            if(CheckNetworkConnection.isURLReachable(MainActivity.this))
                return new GetData().loginUser(email,password);
            else
                return false;
        }

        @Override
        protected void onPostExecute(Object o) {
            super.onPostExecute(o);
            progressDialog.dismiss();
            if(o instanceof Boolean){
                Toast.makeText(MainActivity.this, "No Internet. Please check your network settings", Toast.LENGTH_SHORT).show();
            }else if(o instanceof String){
                String feedback=(String)o;

                if(feedback.equals("success")){
                    startActivity(MembersListActivity.newInstance(MainActivity.this));
                }else{
                    Toast.makeText(MainActivity.this, feedback, Toast.LENGTH_SHORT).show();
                }
            }
        }
    }
}
